/**
 * 	Created by 	: 	Sree Harsha Sake
 * 	Dated 		:	May 20, 2016
 * 	Contact		: 	sreesake@live.com	
 */

package com.sei.sampler;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class DeDup {

	public int[] randomIntegers = {1,2,34,34,25,1,45,3,26,85,4,34,86,25,43,2,1,10000,11,16,19,1,18,4,9,3,20,17,8,15,6,2,5,10,14,12,13,7,8,9,1,2,15,12,18,10,14,20,17,16,3,6,19,13,5,11,4,7,19,16,5,9,12,3,20,7,15,17,10,6,1,8,18,4,14,13,2,11};
	
	/**
	 * Can use Arrays.asList here. 
	 * But it will throw warning saying "Expression of the type Set needs unchecked conversion".
	 * To avoid that we need to cast and add each element to the Set manually.
	 * Casting here is taken care by autoboxing.
	 *
	 * Remove duplicates from the given array using Set implementation from Collections. 
	 * Using LinkedHashSet to maintain insertion order.
	 * 
	 * @param	intArray	integer array with duplicates
	 * @return	int[] 		integer array without duplicates
	 */
	
	public int[] withSet(int[] intArray){
		Set<Integer> intSet = new LinkedHashSet<Integer>();
		for(int i=0; i<intArray.length; i++){
			intSet.add(intArray[i]);
		}
		return setToArray(intSet);
		
	}
	
	/**
	* Method to check if the given array contains the specified element. 
	* 
	* @param	intArray	integer array to be checked against
	* @param 	a			integer to be checked
	* @return	boolean		Returns true if the element is present, returns false if the element is not present.
	*
	*/
	
	public boolean checkArray(final int[] intArray, final int a){
		for(int i : intArray){
			if (i == a){
				return true;
			}
		}
		return false;
	}
	
	/**
	* Method to check and add if the given array contains the specified element.
	* If the array contains the element, return the array.
	* If it doesn't contain the element, then create a dynamic copy of the new array with size+1 and add the element and return the array. 
	* 
	* @param	intArray	integer array to be checked against
	* @param 	a			integer to be checked
	* @return	int[]		Returns integer array with non duplicate entries.
	*
	*/
	
	public int[] checkAndAddToArray(int[] inputArray, int a){
		int[] outputArray = {};
		boolean found = checkArray(inputArray, a);
		if(!found){
			int outputArraySize = inputArray.length+1;
			outputArray = new int[outputArraySize];
			int index = 0;
			for (int i : inputArray){
				outputArray[index] = i;
				index++;
			}
			outputArray[index] = a;
		}
		if(outputArray.length > 0)
			return outputArray;
		else 
			return inputArray;
	}
	
	/**
	* Base method to initiate the process to remove duplicate numbers from the array.
	* 
	* @param	intArray	integer array with duplicates.
	* @return	int[]		integer array without duplicates.
	*
	*/
	
	public int[] withArray(int[] intArray){
		
		int[] resultArray = {};
		
		for(int i : intArray){
			if(resultArray.length == 0){
				resultArray = new int[1];
				resultArray[0] = i;
			} else{
				resultArray = checkAndAddToArray(resultArray, i);
			}
		}
		
		return resultArray;
	}
	
	/**
	* Method to convert set of integers into an array of integers.
	* 
	* @param	Set<Integer> s	Set of integers.
	* @return	int[]			Array of integers.
	*
	*/
	
	public static int[] setToArray(Set<Integer> s){
		int[] resultArray = new int[s.size()];
		int index = 0;
		for (Integer i : s){
			resultArray[index++] = i;
		}
		return resultArray;
	}

	/**
	* Method to remove duplicates from a array of integers by storing them in a map.
	* 
	* @param	int[] inputArray		Set of integers.
	* @return	Map<Integer, Integer>	Map of integers where the keys represent the non duplicate elements of the array, 
	* 									while the corresponding values represent the total number of occurrences 
	* 									of the respective keys in the original array.
	*
	*/
	
	public Map<Integer, Integer> withMap(int[] inputArray){
		
		Map<Integer,Integer> intMap = new HashMap<Integer, Integer>();
		
		for(int i : inputArray){
			if(!intMap.containsKey(i)){
				intMap.put(i, 1);
			} else{
				int occurance = intMap.get(i);
				intMap.put(i, occurance+1);
			}
		}
		
		return intMap;
	}

	/**
	* Method to display the contents of the array.
	* 
	* @param	String resultType	Contains brief description about the contents of the array.
	* @param	int[] intArray		integer array whose contents need to printed onto the console.
	*
	*/
	
	public static void displayResult(String resultType, int[] intArray){
		System.out.println(resultType);
		System.out.println(Arrays.toString(intArray));
	}
	
	public static void main(String [] args) {
		DeDup deDup = new DeDup();
		displayResult("Initial input : ", deDup.randomIntegers);
		int[] firstSol = deDup.withSet(deDup.randomIntegers);
		displayResult("First solution : ", firstSol);
		int[] secondSol = deDup.withArray(deDup.randomIntegers);	
		displayResult("Second solution : ", secondSol);
		Map<Integer, Integer> thirdSol = deDup.withMap(deDup.randomIntegers);	
		displayResult("Third solution : ", setToArray(thirdSol.keySet()));
		
		/* Use case for the third solution : Verify the total number of occurances for each element in the array.
		*
		System.out.println("Occurances for each element in the array are as follows : ");
		System.out.println(thirdSol);
		*/
	}

}